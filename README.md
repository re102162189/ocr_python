cd ocr-tesseract  
docker build -t ocr-tesseract .  

cd ..  
cd ocr-python  
docker build -t ocr-python .  

cd ..  
cd ocr-flask  
docker build -t ocr-flask .  
docker run --name ocr-flask -d -p 80:80 ocr-flask  