from flask import Flask, render_template, request
from werkzeug import secure_filename
from pdf2image import convert_from_path
from PIL import Image
import numpy as np
import os
import sys
import pytesseract
import argparse
import cv2

app = Flask(__name__)
UPLOAD_FOLDER = './static/uploads'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

@app.route("/")
def index():
  return render_template("index.html")

@app.route('/uploader', methods = ['GET', 'POST'])
def upload_file():
   if request.method == 'POST':
      f = request.files['file']
      filename = secure_filename(f.filename)
      filepath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
      f.save(filepath)
      
      convertImg = os.path.join(app.config['UPLOAD_FOLDER'], 'convert.jpg')
      page = convert_from_path(filepath)[0]; 
      page.save(convertImg, 'JPEG')
      image = cv2.imread(convertImg)

      gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) 
      gray = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
      gray = cv2.medianBlur(gray, 3)

      ofilename = os.path.join(app.config['UPLOAD_FOLDER'], "{}.png".format(os.getpid()))
      cv2.imwrite(ofilename, gray)
      
      text = pytesseract.image_to_string(Image.open(ofilename), config='-l eng+chi_tra --oem 1 --psm 3')
      
      os.remove(ofilename)
      os.remove(filepath)

      return render_template("uploaded.html", displaytext=text, fname='convert.jpg')

if __name__ == '__main__':
   app.run(host="0.0.0.0", port=80, debug=True)
